#!/usr/bin/env python3

import glob
import json
import re
import os

dirs = glob.glob('/sys/kernel/debug/qat_*')
dirs.sort()

devices = {}

for dir in dirs:
    key = os.path.basename(dir)
    reqres = { 'Requests ': 0, 'Responses': 0 }
    with open (dir + '/fw_counters', 'rt') as fwcounters:
        for line in fwcounters:
            if line.find('Firmware') > 0:
                m = re.match('.*(Requests |Responses)\[AE\s+(\d)\]:\s+(\d*).*', line)
                reqres[m.group(1)] += int(m.group(3))
    devices[key] = reqres
j = json.dumps(devices, indent=2)
print(j)